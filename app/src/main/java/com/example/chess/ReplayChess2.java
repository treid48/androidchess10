package com.example.chess;

import androidx.annotation.DrawableRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import model.BoardControls;
import model.Piece;
import model.Recording;
import model.RecordingFile;

public class ReplayChess2 extends AppCompatActivity {
    ImageButton a8;
    ImageButton a5;
    ImageButton a4;
    private ImageButton source1;
    private ImageButton source2;
    private int clicks = 0;  //the number of clicks determines if the button being click should be the first or second input
    private String currentInput1;   //currentInput1 represents the original position of a piece trying to be moved
    private String currentInput2;   //currentInput2 represents the desired new location for the piece to move to

    List<String> listOfMoves = new ArrayList<String>();
    RecordingFile list;
    boolean check = false;
    boolean checkMate = false;
    boolean draw = false;
    boolean resign = false;


    /**
     * whitePcs contains all the white chess pieces that are currently
     * on the board.
     * blackPcs contains all the black chess pieces that are currently
     * on the board.
     */
    ArrayList<Piece> whitePcs = new ArrayList<Piece>();
    ArrayList<Piece> blackPcs = new ArrayList<Piece>();

    /**
     * board has the dimension of the chess board.
     * Row is the x coordinate of the chessboard.
     * Column is the y cooordinate of the chessboard
     * Chess board is initialized
     */

    int rows = 8;
    int columns = 8;
    String[][] board = new String[rows][columns];


    String turnColor = "white";

    Recording recording;
    List<String> movesList;
    int iterator;
    int listSize;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_replay_chess2);

        whitePcs = BoardControls.makeWhitePieces();
        blackPcs = BoardControls.makeBlackPieces();
        board = BoardControls.updateBoard(blackPcs, whitePcs);

        Bundle bundle = getIntent().getExtras();
        String title = bundle.getString("Game name");
        //Toast.makeText(ReplayChess2.this, title, Toast.LENGTH_LONG).show();

        try {
            list = RecordingFile.loadList(this);
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }

        if(list == null) {
            finish();
        }
        recording = list.getRecording(title, list);
        movesList = recording.getMoves();
        //Toast.makeText(ReplayChess2.this, movesList.toString(), Toast.LENGTH_LONG).show();
        iterator = 0;
        listSize = movesList.size();

    }



    public void nextMove(View view){
        //Toast.makeText(ReplayChess2.this, "Next Move!", Toast.LENGTH_LONG).show();
        if(iterator >= listSize){

            try {
                RecordingFile.save(ReplayChess2.this, list);
            } catch (IOException exception) {
                exception.printStackTrace();
            }
            finish();

        }
        else{
            String temp1 = movesList.get(iterator); //each elemt is 4 character pair describing a move (e2e4)
            currentInput1 = temp1.substring(0, 2);  //first two characters
            currentInput2 = temp1.substring(2); //last two characters
            boolean success = performMoveAndUpdate(currentInput1, currentInput2);
            iterator += 1;
        }

    }



    /* this is the essential chess algo that moves pieces and updates board
    input1 is the original position of a piece (corresponds with currentInput1)
    input2 is the desired new position for the piece to move to (corresponds with currentInput2) */

    public boolean performMoveAndUpdate(String input1, String input2){

        /**
         * Variable used to determine if the move has completed or not.
         * Needed to know if move was illegal, if it is needed to reask for input,
         * and avoid skipping any team's turn.
         */

        boolean successfulMove = false;


        if(turnColor == "white"){
            for(Piece p: whitePcs){
                if(p.getName() == "wp" || p.getName() == "wB" || p.getName() == "wR" || p.getName() == "wN" || p.getName() == "wQ" || p.getName() == "wK"){
                    p.generatePossibleMoves(p, whitePcs, blackPcs, board);
                    p.generatePossibleAttacks(p, whitePcs, blackPcs, board);
                }
            }

            for(Piece p: whitePcs){
                if(p.getposString().equals(input1)){
                    successfulMove = p.move(input1, input2, turnColor, p, whitePcs, blackPcs);

                }
            }
            if(successfulMove == false){  //if move was correct and succesful move on, if not print incorrect move, prompt for new user input and send back to top of loop
                //Toast.makeText(ChessPage.this, "Unsuccessful move, try again!", Toast.LENGTH_LONG).show();
                clicks = 0;
                return false;
            }
            else{
                updateDrawing(currentInput1, currentInput2);
            }


            for(Piece p: whitePcs){  //see if out king is in check, after our attempted user move
                if(p.getName() == "wK"){
                    if(p.inCheck(turnColor, p, whitePcs, blackPcs, board)){ //if the king, of the color we just tried to move, is now in check because of the move...
                        for(Piece z: whitePcs){
                            if(z.getposString().equals(input2)){     //essentially revert the move of our piece
                                z.setposString(input1);
                                int oldX = z.posGetX_fromString(input1);
                                int oldY = z.posGetY_fromString(input1);
                                z.setXpos(oldX);
                                z.setYpos(oldY);

                            }
                        }
                    }
                }
            }


            for(Piece p: blackPcs){  //see if opposing king is in check, after our attempted user move
                if(p.getName() == "bK"){
                    if(p.inCheck("black", p, whitePcs, blackPcs, board)){ //if the king, of the color we just tried to move, is now in check because of the move...
                        check = true;
                    }
                }
            }


        }



        if(turnColor == "black"){
            for(Piece p: blackPcs){
                if(p.getName() == "bp" || p.getName() == "bB" || p.getName() == "bR" || p.getName() == "bN" || p.getName() == "bQ" || p.getName() == "bK"){
                    p.generatePossibleMoves(p, whitePcs, blackPcs, board);
                    p.generatePossibleAttacks(p, whitePcs, blackPcs, board);
                }

            }
            for(Piece p: blackPcs){
                if(p.getposString().equals(input1)){
                    successfulMove = p.move(input1, input2, turnColor, p, whitePcs, blackPcs);

                }
            }


            if(successfulMove == false) {   //if move was correct and succesful move on, if not print incorrect move, prompt for new user input and send back to top of loop
                //Toast.makeText(ChessPage.this, "Unsuccessful move, try again!", Toast.LENGTH_LONG).show();
                clicks = 0;
                return false;
            }
            else{
                updateDrawing(currentInput1, currentInput2);
            }

            for(Piece p: blackPcs){  //see if out king is in check, after our attempted user move
                if(p.getName() == "bK"){
                    if(p.inCheck(turnColor, p, whitePcs, blackPcs, board)){ //if the king, of the color we just tried to move, is now in check because of the move...
                        for(Piece z: blackPcs){
                            if(z.getposString().equals(input2)){     //essentially revert the move of our piece
                                z.setposString(input1);
                                int oldX = z.posGetX_fromString(input1);
                                int oldY = z.posGetY_fromString(input1);
                                z.setXpos(oldX);
                                z.setYpos(oldY);

                            }
                        }
                    }
                }
            }

            for(Piece p: whitePcs){  //see if opposing king is in check, after our attempted user move
                if(p.getName() == "wK"){
                    if(p.inCheck("white", p, whitePcs, blackPcs, board)){ //if the king, of the color we just tried to move, is now in check because of the move...
                        check = true;
                    }
                }
            }

        }
        /**
         * update the board based on changes
         */


        board = BoardControls.updateBoard(blackPcs, whitePcs);


           /* if(check == true){
                Toast.makeText(com.example.chess.ChessPage.this, "Check!", Toast.LENGTH_LONG).show();
            }*/

        /**
         * Alternate between teams
         */


        if(turnColor == "white"){
            turnColor = "black";
        }
        else {
            turnColor = "white";
        }


        clicks = 0;

        return true;


    }



    public void updateDrawing(String oldPosition, String newPosition){
        int resId = getResources().getIdentifier("z" + oldPosition, "id", getPackageName());
        ImageButton oldImage = (ImageButton) findViewById(resId);

        int resId2 = getResources().getIdentifier("z" + newPosition, "id", getPackageName());
        ImageButton newImage = (ImageButton) findViewById(resId2);

        newImage.setImageDrawable(oldImage.getDrawable());
        oldImage.setImageDrawable(null);
    }




}

