package com.example.chess;
import java.io.Serializable;
import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import model.Recording;

public class RecycleViewAdapter extends RecyclerView.Adapter<RecycleViewAdapter.Holder> {

    private List<Recording> recordingsList;
    private Context context;
    private OnRecordingListener mOnrecordingListener;

    public RecycleViewAdapter(List<Recording> recordingsList, Context context, OnRecordingListener onRecordingListener)
    {
        this.context = context;
        this.recordingsList = recordingsList;
        this.mOnrecordingListener = onRecordingListener;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.session,parent,false);
        return new Holder(view, mOnrecordingListener);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder hold, int rank) {
        hold.recordingTitle.setText(recordingsList.get(rank).getTitle());
        hold.recordingDate.setText(recordingsList.get(rank).getDate());
    }

    @Override
    public int getItemCount() {
        return recordingsList.size();
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView recordingTitle, recordingDate;
        OnRecordingListener onRecordingListener;

        public Holder(@NonNull View sesView, OnRecordingListener onRecordingListener) {
            super(sesView);
            recordingTitle = sesView.findViewById(R.id.recordingTitle);
            recordingDate = sesView.findViewById(R.id.recordingDate);
            this.onRecordingListener = onRecordingListener;

            sesView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onRecordingListener.OnRecordingClick();

        }
    }

    public interface OnRecordingListener{
        void OnRecordingClick();
    }

}