package com.example.chess;
import model.RecordingFile;

import java.io.IOException;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Collections;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import model.Recording;

// page where all the recordings will be listed
public class RecordingPage extends AppCompatActivity implements RecycleViewAdapter.OnRecordingListener {
    private RecyclerView list;
    private RecordingFile result;
    //private List<Recording> recordings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recording);

        // from the recycler view in activity_recording to RecordingFile

        list = findViewById(R.id.list);
        try {
            result = RecordingFile.loadList(this);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        if (result == null) {
            result = new RecordingFile();
        }

        //List<String> lists = Arrays.asList("one", "hi");
        //recordings = new ArrayList<>();
        /*result.add(new Recording("First chess game", Calendar.getInstance().getTime(), lists));
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        result.add(new Recording("Second chess game", Calendar.getInstance().getTime(), lists));
        result.add(new Recording("A chess game", Calendar.getInstance().getTime(), lists));
*/
        //Collections.sort(result.getList(), Recording.RecordingDescendingDate);
        list.setLayoutManager(new LinearLayoutManager(this));
        RecycleViewAdapter adapt = new RecycleViewAdapter(result.getList(), this, this::OnRecordingClick);
        list.setAdapter(adapt);

        try {
            RecordingFile.save(RecordingPage.this, result);
        } catch (IOException exception) {
            exception.printStackTrace();
        }


    }


    @Override
    public void OnRecordingClick() {
        Intent intent = new Intent(this, ReplayChess2.class);
        // Holder holder = findViewById(R,id.)
        TextView tit = findViewById(R.id.recordingTitle);
        String title = tit.getText().toString();
        Bundle bundle = new Bundle();
        // passing the data into the bundle
        bundle.putString("Game name", title);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void titleButton(View v) {
        Collections.sort(result.getList(), Recording.RecordingTitleSortAZ);
        list.setLayoutManager(new LinearLayoutManager(this));
        RecycleViewAdapter adapt = new RecycleViewAdapter(result.getList(), this, this::OnRecordingClick);
        list.setAdapter(adapt);
    }
    public void dateButton(View v) {
        Collections.sort(result.getList(), Recording.RecordingAscendingDate);
        list.setLayoutManager(new LinearLayoutManager(this));
        RecycleViewAdapter adapt = new RecycleViewAdapter(result.getList(), this, this::OnRecordingClick);
        list.setAdapter(adapt);
    }
}
