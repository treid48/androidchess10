package com.example.chess;

import androidx.annotation.DrawableRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;


import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

import model.BoardControls;
import model.Piece;
import model.Recording;
import model.RecordingFile;

public class ChessPage extends AppCompatActivity {
    ImageButton a8;
    ImageButton a5;
    ImageButton a4;
    private ImageButton source1;
    private ImageButton source2;
    private int clicks = 0;  //the number of clicks determines if the button being click should be the first or second input
    private String currentInput1;   //currentInput1 represents the original position of a piece trying to be moved
    private String currentInput2;   //currentInput2 represents the desired new location for the piece to move to

    List<String> listOfMoves = new ArrayList<String>();
    RecordingFile listOfRecordings;
    boolean check = false;
    boolean checkMate = false;
    boolean draw = false;
    boolean resign = false;

    /**
     * whitePcs contains all the white chess pieces that are currently
     * on the board.
     * blackPcs contains all the black chess pieces that are currently
     * on the board.
     */
    ArrayList<Piece> whitePcs = new ArrayList<Piece>();
    ArrayList<Piece> blackPcs = new ArrayList<Piece>();

    /**
     * board has the dimension of the chess board.
     * Row is the x coordinate of the chessboard.
     * Column is the y cooordinate of the chessboard
     * Chess board is initialized
     */

    int rows = 8;
    int columns = 8;
    String[][] board = new String[rows][columns];


    String turnColor = "white";


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        try {
            listOfRecordings = RecordingFile.loadList(this);
        }
        catch(Exception exception) {
            exception.printStackTrace();
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chess_view);

        whitePcs = BoardControls.makeWhitePieces();
        blackPcs = BoardControls.makeBlackPieces();
        board = BoardControls.updateBoard(blackPcs, whitePcs);


    }

    public void onClick(View view) {

        if(clicks == 0){

            source1 = (ImageButton) view;
            String temp1 = getResources().getResourceName(source1.getId());
            currentInput1 = temp1.replace("com.example.chess:id/", "");


            if(source1.getDrawable() == null) {    //check if first click is an empty space with no piece
                //Toast.makeText(ChessPage.this, "Illegal move, try again!", Toast.LENGTH_LONG).show();
                return;
            }
            else
                clicks = 1;//maybe include a another if-statement check to see if first click is on wrong color piece(not their turn)
            //Toast.makeText(ChessPage.this, "First click registered", Toast.LENGTH_LONG).show();
            return;
        }

        if (clicks == 1) {

            source2 = (ImageButton) view;
            String temp2 = getResources().getResourceName(source2.getId());
            currentInput2 = temp2.replace("com.example.chess:id/", "");

            if(source2.getDrawable() == source1.getDrawable()) {   //check if second click is on same coordinate as first click
                clicks = 0;
                //Toast.makeText(ChessPage.this, "Illegal move, try again!", Toast.LENGTH_LONG).show();
                return;
            }
            //Toast.makeText(ChessPage.this, currentInput2, Toast.LENGTH_LONG).show();
            boolean success = performMoveAndUpdate(currentInput1, currentInput2);  //send inputs to chess algo and update images on board

        }
    }


    public void AImove(View view) {
        //Toast.makeText(ChessPage.this,
        if(turnColor == "white"){
            boolean moveMade = false;
            while(moveMade == false){
                    int rand = getRandomNumber(0, whitePcs.size());
                    Piece p = whitePcs.get(rand);
                    currentInput1 = p.getposString();
                    int i = 0;
                    String temp = "";
                    p.generatePossibleMoves(p, whitePcs, blackPcs, board);
                    if(p.getPossMovesList().isEmpty()){
                        continue;
                    }
                    String movesList = p.getPossMovesList();
                    temp += movesList.charAt(i);
                    temp += movesList.charAt(i+1);
                    currentInput2 = temp;
                    moveMade = performMoveAndUpdate(currentInput1, currentInput2);
                    if (moveMade == true) {
                        listOfMoves.add(currentInput1+ currentInput2);
                        p.clearListPossMoves();
                        return;
                        }
                    //Toast.makeText(ChessPage.this, "WORKING", Toast.LENGTH_LONG).show();
            }
            Toast.makeText(ChessPage.this, "WORKING", Toast.LENGTH_LONG).show();
        }

        if(turnColor == "black"){
            boolean moveMade = false;
            while(moveMade == false){
                int rand = getRandomNumber(0, whitePcs.size());
                Piece p = blackPcs.get(rand);
                currentInput1 = p.getposString();
                int i = 0;
                String temp = "";
                p.generatePossibleMoves(p, whitePcs, blackPcs, board);
                if(p.getPossMovesList().isEmpty()){
                    continue;
                }
                String movesList = p.getPossMovesList();
                temp += movesList.charAt(i);
                temp += movesList.charAt(i+1);
                currentInput2 = temp;
                moveMade = performMoveAndUpdate(currentInput1, currentInput2);
                if (moveMade == true) {
                    p.clearListPossMoves();
                    listOfMoves.add(currentInput1+ currentInput2);
                    return;
                }
            }
            Toast.makeText(ChessPage.this, "WORKING", Toast.LENGTH_LONG).show();
        }



    }




    public void undoMove(View view){

        if(turnColor == "white") {
           for (Piece p : blackPcs) {
                // System.out.println(p.getposString());
                if (p.getposString().equals(currentInput2)) {
                    p.setposString(currentInput1);
                    int j = posGetX_fromString(currentInput1);
                    int k = posGetY_fromString(currentInput1);
                    p.setXpos(j);
                    p.setYpos(k);
                    board = BoardControls.updateBoard(blackPcs, whitePcs);
                    updateDrawing(currentInput2, currentInput1);
                    turnColor = "black";
                    listOfMoves.remove(listOfMoves.size() - 1);
                    return;

                }
            }
        }
        if(turnColor == "black") {
            for (Piece p : whitePcs) {
                // System.out.println(p.getposString());
                if (p.getposString().equals(currentInput2)) {
                    p.setposString(currentInput1);
                    int j = posGetX_fromString(currentInput1);
                    int k = posGetY_fromString(currentInput1);
                    p.setXpos(j);
                    p.setYpos(k);
                    board = BoardControls.updateBoard(blackPcs, whitePcs);
                    updateDrawing(currentInput2, currentInput1);
                    turnColor = "white";
                    listOfMoves.remove(listOfMoves.size() - 1);
                    return;


                }
            }
        }

    }


    public void resignGame(View view){


        if(listOfRecordings == null) {
            listOfRecordings = new RecordingFile();
        }

        String titleMessage = "";

        if(turnColor.equals("white")){
            titleMessage = "Game Over: White resigns, Black Wins!";
        }
        else if(turnColor.equals("black")){
            titleMessage = "Game Over: Black resigns, White Wins!";
        }

        final EditText input = new EditText(this);
        final AlertDialog resignDialog = new AlertDialog.Builder(this)
                .setView(input)
                .setTitle(titleMessage)
                .setMessage("Would you like to save your game? If so, enter a name.")
                .setPositiveButton(android.R.string.ok, null) //Set to null. We override the onclick
                .setNegativeButton(android.R.string.cancel, null)
                .create();

        resignDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override

            public void onShow(DialogInterface dialogInterface) {

                Button positiveButton = ((AlertDialog) resignDialog).getButton(AlertDialog.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        // TODO Do something
                        String gameName = input.getText().toString();
                        if (gameName.isEmpty()) {
                            System.out.println("empty");
                            Toast t =  Toast.makeText(getApplicationContext(), "A game name must be entered for recording.", Toast.LENGTH_SHORT);
                            t.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0,0);
                            t.show();
                        } else {

                            resignDialog.dismiss();
                            Recording currRecord = new Recording(gameName, Calendar.getInstance().getTime(), listOfMoves);
                            listOfRecordings.add(currRecord);

                            try {
                                RecordingFile.save(ChessPage.this, listOfRecordings);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            finish();

                        }
                    }
                });

                Button negativeButton = ((AlertDialog) resignDialog).getButton(AlertDialog.BUTTON_NEGATIVE);
                negativeButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        // TODO Do something

                        resignDialog.dismiss();
                        finish();
                    }
                });
            }
        });
        resignDialog.show();

    }

    public void drawGame(View view){


        if(listOfRecordings == null) {
            listOfRecordings = new RecordingFile();
        }

        String titleMessage = "";

        if(turnColor.equals("white")){
            titleMessage = "Game Over: Draw!, White Draws";
        }
        else if(turnColor.equals("black")){
            titleMessage = "Game Over: Draw!, Black Draws";
        }

        final EditText input = new EditText(this);
        final AlertDialog drawDialog = new AlertDialog.Builder(this)
                .setView(input)
                .setTitle(titleMessage)
                .setMessage("Would you like to save your game? If so, enter a name.")
                .setPositiveButton(android.R.string.ok, null) //Set to null. We override the onclick
                .setNegativeButton(android.R.string.cancel, null)
                .create();

        drawDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override

            public void onShow(DialogInterface dialogInterface) {

                Button positiveButton = ((AlertDialog) drawDialog).getButton(AlertDialog.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        // TODO Do something
                        String gameName = input.getText().toString();
                        if (gameName.isEmpty()) {
                            System.out.println("empty");
                            Toast t =  Toast.makeText(getApplicationContext(), "A game name must be entered for recording.", Toast.LENGTH_SHORT);
                            t.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0,0);
                            t.show();
                        } else {

                            drawDialog.dismiss();
                            Recording currRecord = new Recording(gameName, Calendar.getInstance().getTime(), listOfMoves);
                            listOfRecordings.add(currRecord);

                            try {
                                RecordingFile.save(ChessPage.this, listOfRecordings);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            finish();

                        }
                    }
                });

                Button negativeButton = ((AlertDialog) drawDialog).getButton(AlertDialog.BUTTON_NEGATIVE);
                negativeButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        // TODO Do something

                        drawDialog.dismiss();
                        finish();
                    }
                });
            }
        });
        drawDialog.show();
    }




    /* this is the essential chess algo that moves pieces and updates board
    input1 is the original position of a piece (corresponds with currentInput1)
    input2 is the desired new position for the piece to move to (corresponds with currentInput2) */

    public boolean performMoveAndUpdate(String input1, String input2){

        /**
         * Variable used to determine if the move has completed or not.
         * Needed to know if move was illegal, if it is needed to reask for input,
         * and avoid skipping any team's turn.
         */

        boolean successfulMove = false;


        if(turnColor == "white"){
            for(Piece p: whitePcs){
                if(p.getName() == "wp" || p.getName() == "wB" || p.getName() == "wR" || p.getName() == "wN" || p.getName() == "wQ" || p.getName() == "wK"){
                    p.generatePossibleMoves(p, whitePcs, blackPcs, board);
                    p.generatePossibleAttacks(p, whitePcs, blackPcs, board);
                }

            }

            for(Piece p: whitePcs){
                if(p.getposString().equals(input1)){
                    successfulMove = p.move(input1, input2, turnColor, p, whitePcs, blackPcs);

                }
            }
            if(successfulMove == false) {  //if move was correct and succesful move on, if not print incorrect move, prompt for new user input and send back to top of loop
                //Toast.makeText(ChessPage.this, "Unsuccessful move, try again!", Toast.LENGTH_LONG).show();
                clicks = 0;
                return false;
            }
            else{
                updateDrawing(currentInput1, currentInput2);
                listOfMoves.add(input1 + input2);
            }


            for(Piece p: whitePcs){  //see if out king is in check, after our attempted user move
                if(p.getName() == "wK"){
                    if(p.inCheck(turnColor, p, whitePcs, blackPcs, board)){ //if the king, of the color we just tried to move, is now in check because of the move...
                        for(Piece z: whitePcs){
                            if(z.getposString().equals(input2)){     //essentially revert the move of our piece
                                z.setposString(input1);
                                int oldX = z.posGetX_fromString(input1);
                                int oldY = z.posGetY_fromString(input1);
                                z.setXpos(oldX);
                                z.setYpos(oldY);

                            }
                        }
                    }
                }
            }


            for(Piece p: blackPcs){  //see if opposing king is in check, after our attempted user move
                if(p.getName() == "bK"){
                    if(p.inCheck("black", p, whitePcs, blackPcs, board)){ //if the king, of the color we just tried to move, is now in check because of the move...
                        check = true;
                    }
                }
            }


        }



        if(turnColor == "black"){
            for(Piece p: blackPcs){
                if(p.getName() == "bp" || p.getName() == "bB" || p.getName() == "bR" || p.getName() == "bN" || p.getName() == "bQ" || p.getName() == "bK"){
                    p.generatePossibleMoves(p, whitePcs, blackPcs, board);
                    p.generatePossibleAttacks(p, whitePcs, blackPcs, board);
                }

            }
            for(Piece p: blackPcs){
                if(p.getposString().equals(input1)){
                    successfulMove = p.move(input1, input2, turnColor, p, whitePcs, blackPcs);

                }
            }


            if(successfulMove == false){   //if move was correct and succesful move on, if not print incorrect move, prompt for new user input and send back to top of loop
                //Toast.makeText(ChessPage.this, "Unsuccessful move, try again!", Toast.LENGTH_LONG).show();
                clicks = 0;
                return false;
            }
            else{
                updateDrawing(currentInput1, currentInput2);
                listOfMoves.add(input1 + input2);
            }

            for(Piece p: blackPcs){  //see if out king is in check, after our attempted user move
                if(p.getName() == "bK"){
                    if(p.inCheck(turnColor, p, whitePcs, blackPcs, board)) { //if the king, of the color we just tried to move, is now in check because of the move...
                        for (Piece z : blackPcs) {
                            if (z.getposString().equals(input2)) {     //essentially revert the move of our piece
                                z.setposString(input1);
                                int oldX = z.posGetX_fromString(input1);
                                int oldY = z.posGetY_fromString(input1);
                                z.setXpos(oldX);
                                z.setYpos(oldY);

                            }
                        }
                        // Toast.makeText(ChessPage.this, "Illegal move, try again!", Toast.LENGTH_LONG).show();
                    }
                }
            }

            for(Piece p: whitePcs){  //see if opposing king is in check, after our attempted user move
                if(p.getName() == "wK"){
                    if(p.inCheck("white", p, whitePcs, blackPcs, board)){ //if the king, of the color we just tried to move, is now in check because of the move...
                        check = true;
                    }
                }
            }

        }
        /**
         * update the board based on changes
         */

        board = BoardControls.updateBoard(blackPcs, whitePcs);



        if(check == true){
            Toast.makeText(ChessPage.this, "Check!", Toast.LENGTH_LONG).show();
        }


        /**
         * Alternate between teams
         */


        if(turnColor == "white"){
            turnColor = "black";
        }
        else {
            turnColor = "white";
        }


        clicks = 0;

        return true;


    }




    

    public void updateDrawing(String oldPosition, String newPosition){
        int resId = getResources().getIdentifier(oldPosition, "id", getPackageName());
        ImageButton oldImage = (ImageButton) findViewById(resId);

        int resId2 = getResources().getIdentifier(newPosition, "id", getPackageName());
        ImageButton newImage = (ImageButton) findViewById(resId2);

        newImage.setImageDrawable(oldImage.getDrawable());
        oldImage.setImageDrawable(null);
    }

    public int posGetX_fromString(String position){
        int temp = 0;
        if(position.charAt(0) == 'a'){
            temp = 0;
        }
        else if(position.charAt(0) == 'b'){
            temp = 1;
        }
        else if(position.charAt(0) == 'c'){
            temp = 2;
        }
        else if(position.charAt(0) == 'd'){
            temp = 3;
        }
        else if(position.charAt(0) == 'e'){
            temp = 4;
        }
        else if(position.charAt(0) == 'f'){
            temp = 5;
        }
        else if(position.charAt(0) == 'g'){
            temp = 6;
        }
        else if(position.charAt(0) == 'h'){
            temp = 7;
        }
        return temp;
    }

    public int posGetY_fromString(String position){
        int temp = 0;

        if(position.charAt(1) == '1'){
            temp = 0;
        }
        else if(position.charAt(1) == '2'){
            temp = 1;
        }
        else if(position.charAt(1) == '3'){
            temp = 2;
        }
        else if(position.charAt(1) == '4'){
            temp = 3;
        }
        else if(position.charAt(1) == '5'){
            temp = 4;
        }
        else if(position.charAt(1) == '6'){
            temp = 5;
        }
        else if(position.charAt(1) == '7'){
            temp = 6;
        }
        else if(position.charAt(1) == '8'){
            temp = 7;
        }
        return temp;
    }

    public int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }






}