package com.example.chess;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class HomePage extends AppCompatActivity {

    Button play_button, recordings_button;
    TextView greeting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);


        play_button = findViewById(R.id.play_button);
        recordings_button = findViewById(R.id.recordings_button);
        greeting = findViewById(R.id.greeting);


    }

    public void goToPlay(View view){
        play_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomePage.this, ChessPage.class));
            }
        });

    }

    public void goToRecordings(View view){
        recordings_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomePage.this, RecordingPage.class));
            }
        });

    }




}