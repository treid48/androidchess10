package model;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.chess.R;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class RecordingFile implements Serializable {
    private static final long serialVersionUID = 1L;

    private List<Recording> recordedGamesList;

    public RecordingFile() {
        recordedGamesList = new ArrayList<>();
    }

    public Recording getRecording(String title, RecordingFile list) {
        for(int i = 0; i < list.getList().size(); i ++) {
            if (list.getList().get(i).getTitle().equals(title)) {
                return list.getList().get(i);
            }
        }
        return null;
    }
    public void add(Recording r) {
        recordedGamesList.add(r);
    }

    public List<Recording> getList() {
        return recordedGamesList;
    }

    public static RecordingFile loadList(Context context) throws IOException, ClassNotFoundException {
        FileInputStream fileInput = context.openFileInput("RecordedGamesList.dat");
        ObjectInputStream objectInput = new ObjectInputStream(fileInput);
        RecordingFile games = (RecordingFile) objectInput.readObject();
        fileInput.close();
        objectInput.close();
        return games;
    }

    public static void save(Context context, RecordingFile newList) throws IOException {
        FileOutputStream fileOutput = context.openFileOutput("RecordedGamesList.dat", Context.MODE_PRIVATE);
        ObjectOutputStream objectOutput = new ObjectOutputStream(fileOutput);
        objectOutput.writeObject(newList);
        objectOutput.close();
        fileOutput.close();
    }

}
