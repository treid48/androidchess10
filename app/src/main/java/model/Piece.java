package model;

import java.util.ArrayList;
/*
 * @author David Zhu, Tim Reid
 * @param name the chess piece role
 * @param color the player's side
 * @param posString the chess board position of the piece
 * @param xPos the x-coordinate/row number of the 2-d array for that piece based off the chess board
 * @param yPos the y-coordinate/row number of the 2-d array for that piece based off the chess board
 */
public abstract class Piece {
    /**
     * gets its role
     */
    String name;
    /**
     * the player's side
     */
    String color;
    /**
     * the chess board position of the piece
     */
    String posString;
    /**
     * holds old position of the chess piece in case user move is illegal
     */
    String oldPos;

    /**
     * gets its current x coordinate
     */
    int xPos;
    /**
     * gets its current y coordinate
     */
    int yPos;

    /**
     * possible positions piece can move to
     */
    String listPossMoves = "";
    /**
     * possible positions piece can replace
     */
    String listPossAttacks = "";
    /**
     * @param name the chess piece role
     * @param color the player's side
     * @param posString the chess board position of the piece
     * @param xPos the x-coordinate/row number of the 2-d array for that piece based off the chess board
     * @param yPos the y-coordinate/row number of the 2-d array for that piece based off the chess board
     */
    public Piece(String name, String color, String posString, int xPos, int yPos){  //basic cosntructor
        this.name = name;
        this.color = color;
        this.posString = posString;
        this.xPos = xPos;
        this.yPos = yPos;
        oldPos = "00";
    }
    /**
     * Uses either the possible move list to move selected chess piece
     * to new position or use the possible attack list to replace
     * a opposing chess piece
     * <p>
     * @param currentPos the position of the piece that is attacking
     * @param targetPos the position of the piece getting attacked
     * @param colorTurn the attacking player's side
     * @param p the current chess piece
     * @param whitePcs array of white pieces
     * @param blackPcs array of black pieces
     * @return true if piece has been moved
     */
    public abstract boolean move(String currentPos, String targetPos, String colorTurn, Piece p, ArrayList<Piece> whitePcs, ArrayList<Piece> blackPcs);
    /**
     * Generate a list containing the possible chess board positions the
     * pawn can move to.
     * <p>
     * @param p the chess piece role, like pawn "bp" or "wp"
     * @param whiteP the list of white pieces present
     * @param blackP the list of black pieces present
     * @param chBoard the 2d-array chessboard
     */
    public abstract void generatePossibleMoves(Piece p, ArrayList<Piece> whiteP, ArrayList<Piece> blackP, String[][] chBoard);
    /**
     * List of possible targets to take out on the chess board.
     * Checks if it is opposite colors by using legal movement on the board.
     * <p>
     * @param p the piece that is attacking
     * @param whiteP array of white pieces
     * @param blackP array of black pieces
     * @param chBoard 2d array that is the chess board
     */
    public abstract void generatePossibleAttacks(Piece p, ArrayList<Piece> whiteP, ArrayList<Piece> blackP, String[][] chBoard);

    /**
     * Gets the position on the chess board using the 2d-array x and y coordinates.
     * <p>
     * @param xCoor x-coordinate of the 2d-array
     * @param yCoor y-coordindate of the 2d-array
     * @return the chess board position
     */
    public String getStringPos_fromXY(int xCoor, int yCoor){
        String temp = "";

        if(xCoor == 0){
            temp = temp + "a";
        }
        else if(xCoor == 1){
            temp = temp + "b";
        }
        else if(xCoor == 2){
            temp = temp + "c";
        }
        else if(xCoor == 3){
            temp = temp + "d";
        }
        else if(xCoor == 4){
            temp = temp + "e";
        }
        else if(xCoor == 5){
            temp = temp + "f";
        }
        else if(xCoor == 6){
            temp = temp + "g";
        }
        else if(xCoor == 7){
            temp = temp + "h";
        }

        if(yCoor == 0){
            temp = temp + "1";
        }
        else if(yCoor == 1){
            temp = temp + "2";
        }
        else if(yCoor == 2){
            temp = temp + "3";
        }
        else if(yCoor == 3){
            temp = temp + "4";
        }
        else if(yCoor == 4){
            temp = temp + "5";
        }
        else if(yCoor == 5){
            temp = temp + "6";
        }
        else if(yCoor == 6){
            temp = temp + "7";
        }
        else if(yCoor == 7){
            temp = temp + "8";
        }

        return temp;
    }
    /**
     * Convert first char of string postion to X coordinate
     * <p>
     * @param position the chess position
     * @return the x-coordinate
     */
    public int posGetX_fromString(String position){
        int temp = 0;
        if(position.charAt(0) == 'a'){
            temp = 0;
        }
        else if(position.charAt(0) == 'b'){
            temp = 1;
        }
        else if(position.charAt(0) == 'c'){
            temp = 2;
        }
        else if(position.charAt(0) == 'd'){
            temp = 3;
        }
        else if(position.charAt(0) == 'e'){
            temp = 4;
        }
        else if(position.charAt(0) == 'f'){
            temp = 5;
        }
        else if(position.charAt(0) == 'g'){
            temp = 6;
        }
        else if(position.charAt(0) == 'h'){
            temp = 7;
        }
        return temp;
    }
    /**
     * Convert first char of string postion to Y coordinate
     * <p>
     * @param position the chess position
     * @return the y-coordinate
     */
    public int posGetY_fromString(String position){
        int temp = 0;

        if(position.charAt(1) == '1'){
            temp = 0;
        }
        else if(position.charAt(1) == '2'){
            temp = 1;
        }
        else if(position.charAt(1) == '3'){
            temp = 2;
        }
        else if(position.charAt(1) == '4'){
            temp = 3;
        }
        else if(position.charAt(1) == '5'){
            temp = 4;
        }
        else if(position.charAt(1) == '6'){
            temp = 5;
        }
        else if(position.charAt(1) == '7'){
            temp = 6;
        }
        else if(position.charAt(1) == '8'){
            temp = 7;
        }
        return temp;
    }


    /**
     * get the name of the chess piece
     * <p>
     * @return chess position
     */
    public String getName() {
        return name;
    }
    /**
     * get the color of chess piece
     * <p>
     * @return team color
     */
    public String getColor() {
        return color;
    }
    /**
     * get the chess board position of the chess piece
     * <p>
     * @return string
     */
    public String getposString() {
        return posString;
    }
    /**
     * get the previous chess board position of the chess piece
     * <p>
     * @return previous chess board position
     */
    public String getOldPos() {
        return oldPos;
    }
    /**
     * get the x-coordinate
     * <p>
     * @return x-coordinate
     */
    public int getXpos() {
        return xPos;
    }
    /**
     * get the y-coordinate
     * <p>
     * @return y-coordinate
     */
    public int getYpos() {
        return yPos;
    }
    /**
     * get the list of possible chess moves
     * <p>
     * @return list
     */
    public String getPossMovesList() {
        return listPossMoves;
    }
    /**
     * get the list of possible chess position replacement
     * <p>
     * @return list
     */
    public String getPossAttacksList() {
        return listPossAttacks;
    }


    /**
     * set the role of the chess piece
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * set the color of the chess piece
     */
    public void setColor(String color) {
        this.color = color;
    }
    /**
     * set the chess board position of the chess piece
     */
    public void setposString(String posString) {
        this.posString = posString;
    }
    /**
     * set the old chess board position of the chess piece
     */
    public void setOldPos(String oldPos) {
        this.oldPos = oldPos;
    }
    /**
     * set the x-coordinate of the chess piece
     */
    public void setXpos(int xPos) {
        this.xPos = xPos;
    }
    /**
     * set the y-coordinate of the chess piece
     */
    public void setYpos(int yPos) {
        this.yPos = yPos;
    }
    /**
     * set a list of possible moves chess piece can go to
     */
    public void setListPossMoves(String temp) {
        listPossMoves = listPossMoves + temp;
    }
    /**
     * clear list
     */
    public void clearListPossMoves() {
        listPossMoves = "";
    }
    /**
     * set a list of possible chess board replacements of that chess piece
     */
    public void setListPossAttacks(String temp) {
        listPossAttacks = listPossAttacks + temp;
    }
    /**
     * clear list
     */
    public void clearListPossAttacks() {
        listPossAttacks = "";
    }

    /**
     * Checks if it has the oppenent in check
     * <p>
     * @param colorTurn player team
     * @param p chess piece
     * @param whitePcs all white chess pieces
     * @param blackPcs all black chess pieces
     * @param chBoard the chess board
     * @return whether the chess piece has the opponent in check
     */

    public boolean inCheck(String colorTurn, Piece p, ArrayList<Piece> whitePcs,
                           ArrayList<Piece> blackPcs, String[][] chBoard){
        //System.out.println(p.getposString() + "----");
        String currentSTR = p.getposString();
        String pieceColor = p.getColor();
        if(pieceColor != colorTurn){  //target piece's color is not same as current color turn
            return false;
        }
        /**
         * Sets the target position as the current position if the target position is in the possilbe list
         */

        /**
         * if
         */
        if(colorTurn.equals("white")){
            for(Piece x: blackPcs){
                generatePossibleAttacks(x, whitePcs, blackPcs, chBoard);
                if(x.getPossAttacksList().contains(currentSTR)){
                    x.clearListPossAttacks();;
                    return true;
                }
                x.clearListPossAttacks();
            }
        }
        if(colorTurn.equals("black")){
            for(Piece x: whitePcs){
                generatePossibleAttacks(x, whitePcs, blackPcs, chBoard);
                if(x.getPossAttacksList().contains(currentSTR)){
                    x.clearListPossAttacks();
                    return true;
                }
                x.clearListPossAttacks();
            }
        }

        return false;

    }


    public boolean inCheckmate(String colorTurn, Piece p, ArrayList<Piece> whitePcs,
                               ArrayList<Piece> blackPcs, String[][] chBoard){
        //System.out.println(p.getposString() + "----");
        int kingSafety = 0;
        int checkMateCount = 0;
        String currentSTR = p.getposString();
        String pieceColor = p.getColor();
        if(pieceColor != colorTurn){  //target piece's color is not same as current color turn
            return false;
        }
        /*
            Sets the target position as the current position if the target position is in the possilbe list
        */


        if(colorTurn.equals("white")){

            p.generatePossibleMoves(p, whitePcs, blackPcs, chBoard);  //generate all of King's possible moves (escapes)

            for(Piece x: blackPcs){
                generatePossibleAttacks(x, whitePcs, blackPcs, chBoard);   //generate and search through every opposite color piece's attack list
                for(int i = 0; i<p.getPossMovesList().length(); i+=2){
                    String parseMoves = p.getPossMovesList().substring(i, i +2);
                    if(x.getPossAttacksList().contains(parseMoves)){  //keep track of how many times a king's potential escape is found in an attack list
                        kingSafety++;
                    }
                }

            }

            if(kingSafety == p.getPossMovesList().length()){  //if every move of king's escape is in an attack list, the first condition for checkmate is satisfied
                checkMateCount++;
            }

            int check1 = 0;
            int check2 = 0;

            for(Piece x: blackPcs){     //if same color piece can kill the attacking color pirce trying to kill king
                if(x.getPossAttacksList().contains(currentSTR)){
                    for(Piece y: whitePcs){
                        y.generatePossibleAttacks(y, whitePcs, blackPcs, chBoard);
                        if(y.getPossAttacksList().contains(x.getposString())){
                            check2++;
                        }
                        y.clearListPossAttacks();
                    }
                    check1++;
                }
                x.clearListPossAttacks();
            }

            if(check1 > check2){  //if there are more attacks than can be elimiated by our pieces, checkmate condition 2 is achieved
                checkMateCount++;
            }

            p.clearListPossAttacks();
            p.clearListPossMoves();


            if(checkMateCount == 2){
                return true;
            }

        }



        if(colorTurn.equals("black")){

            //Piece tempKing = new Piece()
            /*
            for(Piece q: blackPcs){   //locate king and set to temporary Piece
                q.generatePossibleAttacks(q, whitePcs, blackPcs, chBoard);  //generate all possible white attacks for use later
                if(q.getName().equals("wK")){
                Piece tempKing = new Piece(q);
                }
            }
            */
            p.generatePossibleMoves(p, whitePcs, blackPcs, chBoard);  //generate all of King's possible moves (escapes)

            for(Piece x: whitePcs){
                generatePossibleAttacks(x, whitePcs, blackPcs, chBoard);   //generate and search through every opposite color piece's attack list
                for(int i = 0; i<p.getPossMovesList().length(); i+=2){
                    String parseMoves = p.getPossMovesList().substring(i, i +2);
                    if(x.getPossAttacksList().contains(parseMoves)){  //keep track of how many times a king's potential escape is found in an attack list
                        kingSafety++;
                    }
                }

            }

            if(kingSafety == p.getPossMovesList().length()){  //if every move of king's escape is in an attack list, the first condition for checkmate is satisfied
                checkMateCount++;
            }

            int check1 = 0;
            int check2 = 0;

            for(Piece x: whitePcs){     //if same color piece can kill the attacking color pirce trying to kill king
                if(x.getPossAttacksList().contains(currentSTR)){
                    for(Piece y: blackPcs){
                        y.generatePossibleAttacks(y, whitePcs, blackPcs, chBoard);
                        if(y.getPossAttacksList().contains(x.getposString())){
                            check2++;
                        }
                        y.clearListPossAttacks();

                    }
                    check1++;
                }
                x.clearListPossAttacks();
            }

            if(check1 > check2){  //if there are more attacks than can be elimiated by our pieces, checkmate condition 2 is achieved
                checkMateCount++;
            }

            p.clearListPossAttacks();
            p.clearListPossMoves();

            if(checkMateCount == 2){
                return true;
            }

        }

        return false;

    }


}
