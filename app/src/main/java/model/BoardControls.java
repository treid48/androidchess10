package model;

import java.util.ArrayList;

public class BoardControls {

    public static ArrayList<Piece> makeBlackPieces(){

        ArrayList<Piece> pieceList = new ArrayList<Piece>();

        Pawn pawn1 = new Pawn("bp", "black", "a7", 0, 6);
        pieceList.add(pawn1);
        Pawn pawn2 = new Pawn("bp", "black", "b7", 1, 6);
        pieceList.add(pawn2);
        Pawn pawn3 = new Pawn("bp", "black", "c7", 2, 6);
        pieceList.add(pawn3);
        Pawn pawn4 = new Pawn("bp", "black", "d7", 3, 6);
        pieceList.add(pawn4);
        Pawn pawn5 = new Pawn("bp", "black", "e7", 4, 6);
        pieceList.add(pawn5);
        Pawn pawn6 = new Pawn("bp", "black", "f7", 5, 6);
        pieceList.add(pawn6);
        Pawn pawn7 = new Pawn("bp", "black", "g7", 6, 6);
        pieceList.add(pawn7);
        Pawn pawn8 = new Pawn("bp", "black", "h7", 7, 6);
        pieceList.add(pawn8);

        Rook rook1 = new Rook("bR", "black", "a8", 0, 7);
        pieceList.add(rook1);
        Rook rook2 = new Rook("bR", "black", "h8", 7, 7);
        pieceList.add(rook2);

        Bishop bishop1 = new Bishop("bB", "black", "c8", 2, 7);
        pieceList.add(bishop1);
        Bishop bishop2 = new Bishop("bB", "black", "f8", 5, 7);
        pieceList.add(bishop2);

        Knight knight1 = new Knight("bN", "black", "b8", 1, 7);
        pieceList.add(knight1);
        Knight knight2 = new Knight("bN", "black", "g8", 6, 7);
        pieceList.add(knight2);

        Queen queen1 = new Queen("bQ", "black", "d8", 3, 7);
        pieceList.add(queen1);

        King king2 = new King("bK", "black", "e8", 4, 7);
        pieceList.add(king2);

        return pieceList;

    }


    public static ArrayList<Piece> makeWhitePieces(){

        ArrayList<Piece> pieceList = new ArrayList<Piece>();

        Pawn pawn1 = new Pawn("wp", "white", "a2", 0, 1);
        pieceList.add(pawn1);
        Pawn pawn2 = new Pawn("wp", "white", "b2", 1, 1);
        pieceList.add(pawn2);
        Pawn pawn3 = new Pawn("wp", "white", "c2", 2, 1);
        pieceList.add(pawn3);
        Pawn pawn4 = new Pawn("wp", "white", "d2", 3, 1);
        pieceList.add(pawn4);
        Pawn pawn5 = new Pawn("wp", "white", "e2", 4, 1);
        pieceList.add(pawn5);
        Pawn pawn6 = new Pawn("wp", "white", "f2", 5, 1);
        pieceList.add(pawn6);
        Pawn pawn7 = new Pawn("wp", "white", "g2", 6, 1);
        pieceList.add(pawn7);
        Pawn pawn8 = new Pawn("wp", "white", "h2", 7, 1);
        pieceList.add(pawn8);

        Rook rook1 = new Rook("wR", "white", "a1", 0, 0);
        pieceList.add(rook1);
        Rook rook2 = new Rook("wR", "white", "h1", 7, 0);
        pieceList.add(rook2);

        Bishop bishop1 = new Bishop("wB", "white", "c1", 2, 0);
        pieceList.add(bishop1);
        Bishop bishop2 = new Bishop("wB", "white", "f1", 5, 0);
        pieceList.add(bishop2);

        Knight knight1 = new Knight("wN", "white", "b1", 1, 0);
        pieceList.add(knight1);
        Knight knight2 = new Knight("wN", "white", "g1", 6, 0);
        pieceList.add(knight2);

        Queen queen1 = new Queen("wQ", "white", "d1", 3, 0);
        pieceList.add(queen1);

        King king2 = new King("wK", "white", "e1", 4, 0);
        pieceList.add(king2);

        return pieceList;

    }

    public static String[][] updateBoard(ArrayList<Piece> blackPcs, ArrayList<Piece> whitePcs){
        int rows = 8;
        int columns = 8;
        String[][] board = new String[rows][columns];

        for(int i = 0; i<8; i++){
            for(int j = 7; j>=0; j--){

                if (i%2 == 1){
                    if(j%2 == 0){
                        board[i][j] = "  ";
                    }
                    else{
                        board[i][j] = "##";
                    }
                }
                if(i%2 == 0){
                    if(j%2 == 1){
                        board[i][j] = "  ";
                    }
                    else{
                        board[i][j] = "##";
                    }
                }
            }
        }

        for(int i = 0; i<rows; i++){
            for(int j = 0; j<columns; j++){
                for(Piece p: whitePcs){
                    if(i == p.getXpos() && j == p.getYpos()){
                        board[i][j] = p.getName();
                    }
                }
                for(Piece p: blackPcs){
                    if(i == p.getXpos() && j == p.getYpos()){
                        board[i][j] = p.getName();
                    }
                }

            }
        }

        return board;



    }


    public static void printBoard(String[][] board){
        System.out.println();
        int temp = 8;

        for(int j = 7; j>=0; j--){     //***note special order through for loop to print correct format
            for(int i = 0; i<8; i++){
                System.out.print(board[i][j] + " ");
            }
            System.out.print(temp);
            temp--;
            System.out.println();
        }
        System.out.print(" a  b  c  d  e  f  g  h\n");
    }



}
