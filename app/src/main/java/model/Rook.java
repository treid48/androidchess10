package model;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author David Zhu, Tim Reid
 * The Rook class where we design how the pawn interacts with others chess pieces
 * and it's behavior, attributes and limitations.
 *
 */

public class Rook extends Piece {

    /**
     * @param name the chess piece role
     * @param color the player's side
     * @param posString the chess board position of the piece
     * @param xPos the x-coordinate/row number of the 2-d array for that piece based off the chess board
     * @param yPos the y-coordinate/row number of the 2-d array for that piece based off the chess board
     */
    public Rook(String name, String color, String posString, int xPos, int yPos){  //basic cosntructor
        super(name, color, posString, xPos, yPos);
        oldPos = "00";
    }

    /**
     * Generate a list containing the possible chess board positions the
     * Rook can move to.
     * <p>
     * @param p the chess piece
     * @param whiteP list of white pieces present on the board
     * @param blackP list of black pieces present on the board
     * @param chBoard the 2d-array chessboard
     */
    public void generatePossibleMoves(Piece p, ArrayList<Piece> whiteP, ArrayList<Piece> blackP, String[][] chBoard){
        /**
         * gets its position on the chess board
         */
        String currentSTR = p.getposString();
        /**
         * gets its color
         */
        String color = p.getColor();
        /**
         * gets its current x coordinate
         */
        int currentX = posGetX_fromString(currentSTR);
        /**
         * gets its current y coordinate
         */
        int currentY = posGetY_fromString(currentSTR);
        /**
         * gets the position on the chessboard that is legal for the current
         * pawn to move to
         */
        String temp = "";
        /**
         * For Rook going up
         */
        for(int j = (currentY + 1), i = currentX; j < 8; j++) {
            if(chBoard[i][j].equals("  ") || chBoard[i][j].equals("##")){
                temp = getStringPos_fromXY(i, j);
                p.setListPossMoves(temp);
            } else {
                break;
            }
        }
        /**
         * For Rook going down
         */
        for(int j = (currentY - 1), i = currentX; j >= 0; j--) {
            if(chBoard[i][j].equals("  ") || chBoard[i][j].equals("##")){
                temp = getStringPos_fromXY(i, j);
                p.setListPossMoves(temp);
            } else {
                break;
            }
        }
        /**
         * For Rook going left
         */
        for(int i = (currentX - 1), j = currentY; i >= 0; i--) {
            if(chBoard[i][j].equals("  ") || chBoard[i][j].equals("##")){
                temp = getStringPos_fromXY(i, j);
                p.setListPossMoves(temp);
            } else {
                break;
            }
        }
        /**
         * For Rook going right
         */
        for(int i = (currentX + 1), j = currentY; i < 8; i++) {
            if(chBoard[i][j].equals("  ") || chBoard[i][j].equals("##")){
                temp = getStringPos_fromXY(i, j);
                p.setListPossMoves(temp);
            } else {
                break;
            }
        }
    }
    /**
     * List of possible targets to take out on the chess board.
     * Checks if it is opposite colors by using legal movement on the board.
     *<p>
     * @param p the piece that is attacking
     * @param whiteP array of white pieces
     * @param blackP array of black pieces
     * @param chBoard 2d array that is the chess board
     */
    public void generatePossibleAttacks(Piece p, ArrayList<Piece> whiteP, ArrayList<Piece> blackP, String[][] chBoard){
        String currentSTR = p.getposString(); // String placement on board
        String color = p.getColor();
        String temp = "";
        int currentX = posGetX_fromString(currentSTR);
        int currentY = posGetY_fromString(currentSTR);
        /**
         * For Rook going up
         */
        for(int j = (currentY + 1), i = currentX; j < 8; j++) {
            if(color.equals("white") && (chBoard[i][j].equals("bp") || chBoard[i][j].equals("bN") || chBoard[i][j].equals("bB") || chBoard[i][j].equals("bQ") || chBoard[i][j].equals("bK") || chBoard[i][j].equals("bR")  )){
                temp = getStringPos_fromXY(i, j);
                p.setListPossAttacks(temp);
                break;
            } else if ( color.equals("black") && (chBoard[i][j].equals("wp") || chBoard[i][j].equals("wN") || chBoard[i][j].equals("wB") || chBoard[i][j].equals("wQ") || chBoard[i][j].equals("wK") || chBoard[i][j].equals("wR") ) ){
                temp = getStringPos_fromXY(i, j);
                p.setListPossAttacks(temp);
                break;
            }
        }
        /**
         * For Rook going down
         */
        for(int j = (currentY - 1), i = currentX; j >= 0; j--) {
            if(color.equals("white") && (chBoard[i][j].equals("bp") || chBoard[i][j].equals("bN") || chBoard[i][j].equals("bB") || chBoard[i][j].equals("bQ") || chBoard[i][j].equals("bK") || chBoard[i][j].equals("bR")  )){
                temp = getStringPos_fromXY(i, j);
                p.setListPossAttacks(temp);
                break;
            } else if ( color.equals("black") && (chBoard[i][j].equals("wp") || chBoard[i][j].equals("wN") || chBoard[i][j].equals("wB") || chBoard[i][j].equals("wQ") || chBoard[i][j].equals("wK") || chBoard[i][j].equals("wR") ) ){
                temp = getStringPos_fromXY(i, j);
                p.setListPossAttacks(temp);
                break;
            }
        }
        /**
         * For Rook going left
         */
        for(int i = (currentX - 1), j = currentY; i >= 0; i--) {
            if(color.equals("white") && (chBoard[i][j].equals("bp") || chBoard[i][j].equals("bN") || chBoard[i][j].equals("bB") || chBoard[i][j].equals("bQ") || chBoard[i][j].equals("bK") || chBoard[i][j].equals("bR")  )){
                temp = getStringPos_fromXY(i, j);
                p.setListPossAttacks(temp);
                break;
            } else if ( color.equals("black") && (chBoard[i][j].equals("wp") || chBoard[i][j].equals("wN") || chBoard[i][j].equals("wB") || chBoard[i][j].equals("wQ") || chBoard[i][j].equals("wK") || chBoard[i][j].equals("wR") ) ){
                temp = getStringPos_fromXY(i, j);
                p.setListPossAttacks(temp);
                break;
            }
        }
        /**
         *For Rook going right
         */
        for(int i = (currentX + 1), j = currentY; i < 8; i++) {
            if(color.equals("white") && (chBoard[i][j].equals("bp") || chBoard[i][j].equals("bN") || chBoard[i][j].equals("bB") || chBoard[i][j].equals("bQ") || chBoard[i][j].equals("bK") || chBoard[i][j].equals("bR")  )){
                temp = getStringPos_fromXY(i, j);
                p.setListPossAttacks(temp);
                break;
            } else if ( color.equals("black") && (chBoard[i][j].equals("wp") || chBoard[i][j].equals("wN") || chBoard[i][j].equals("wB") || chBoard[i][j].equals("wQ") || chBoard[i][j].equals("wK") || chBoard[i][j].equals("wR") ) ){
                temp = getStringPos_fromXY(i, j);
                p.setListPossAttacks(temp);
                break;
            }
        }

    }

    /**
     * Uses either the possible move list to move selected chess piece
     * to new position or use the possible attack list to replace
     * a opposing chess piece
     * <p>
     * @param currentPos the position of the piece that is attacking
     * @param targetPos the position of the piece getting attacked
     * @param colorTurn the attacking player's side
     * @param p the current chess piece
     * @param whitePcs array of white pieces
     * @param blackPcs array of black pieces
     * @return true if piece has been moved
     */
    public boolean move(String currentPos, String targetPos, String colorTurn, Piece p, ArrayList<Piece> whitePcs,
                        ArrayList<Piece> blackPcs){
        String pieceColor = p.getColor();
        if(pieceColor != colorTurn){  //target piece's color is not same as current color turn
            return false;
        }
        /*
         * Sets the target position as the current position if the target position is in the possilbe list
         */
        if(p.getPossMovesList().contains(targetPos)){
            p.setOldPos(currentPos);
            p.setposString(targetPos);
            int j = posGetX_fromString(targetPos);
            int k = posGetY_fromString(targetPos);
            p.setXpos(j);
            p.setYpos(k);
            p.clearListPossMoves();
            return true;
        }

        if(p.getPossAttacksList().contains(targetPos)){


            if(colorTurn.equals("white")){

                Iterator<Piece> itr = blackPcs.iterator();
                while (itr.hasNext()) {
                    Piece z = itr.next();
                    if (z.getposString().equals(targetPos))  {
                        itr.remove();
                    }
                }


            }
            else if (colorTurn.equals("black")){
                Iterator<Piece> itr2 = whitePcs.iterator();
                while (itr2.hasNext()) {
                    Piece y = itr2.next();
                    if (y.getposString().equals(targetPos))  {
                        itr2.remove();
                    }
                }

            }

            p.setOldPos(currentPos);
            p.setposString(targetPos);
            int a = posGetX_fromString(targetPos);
            int b = posGetY_fromString(targetPos);
            p.setXpos(a);
            p.setYpos(b);
            p.clearListPossAttacks();



            return true;
        }

        return false;
    }

}


