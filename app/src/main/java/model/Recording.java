package model;
import android.content.Context;

import java.io.*;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

// class for each recorded match
// class needed for sorting
public class Recording implements Serializable {
    private static final long serialVersionUID = 1L;
    private String title;
    private Date recordedDate;
    private List<String> moves;

    public Recording(String title, Date recordedDate, List<String> moves) {
        this.recordedDate = recordedDate;
        this.moves = moves;
        this.title = title;
    }

    // recording that is return > 0 is alphabetically larger
    public static Comparator<Recording> RecordingTitleSortAZ = new Comparator<Recording>() {
        @Override
        public int compare(Recording r1, Recording r2) {
            return r1.getTitle().compareTo(r2.getTitle());
        }
    };

    public static Comparator<Recording> RecordingTitleSortZA = new Comparator<Recording>() {
        @Override
        public int compare(Recording r1, Recording r2) {
            return r2.getTitle().compareTo(r1.getTitle());
        }
    };
    // recording that is return > 0 is later than the other date
    public static Comparator<Recording> RecordingAscendingDate = new Comparator<Recording>() {
        @Override
        public int compare(Recording r1, Recording r2) {
            return r1.getDate().compareTo(r2.getDate());
        }
    };
    public static Comparator<Recording> RecordingDescendingDate = new Comparator<Recording>() {
        @Override
        public int compare(Recording r1, Recording r2) {
            return r2.getDate().compareTo(r1.getDate());
        }
    };

    public String getTitle() {
        return title;
    }
    public List<String> getMoves() { return moves; }
    public String getDate() { return recordedDate.toString(); }

}
