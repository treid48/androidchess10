package model;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author David Zhu, Tim Reid
 * The pawn class where we design how the pawn interacts with others chess pieces
 * and it's behavior, attributes and limitations.
 *
 */
public class Pawn extends Piece {

    /**
     * @param name the chess piece role, like pawn "bp" or "wp"
     * @param color the player's side
     * @param posString the chess board position of the piece
     * @param xPos the x-coordinate/row number of the 2-d array for that piece based off the chess board
     * @param yPos the y-coordinate/row number of the 2-d array for that piece based off the chess board
     */

    public Pawn(String name, String color, String posString, int xPos, int yPos){  //basic cosntructor
        super(name, color, posString, xPos, yPos);
        oldPos = "00";
    }

    /**
     * Generate a list containing the possible chess board positions the
     * pawn can move to.
     * <p>
     * @param p the chess piece role, like pawn "bp" or "wp"
     * @param whiteP the list of white pieces present
     * @param blackP the list of black pieces present
     * @param chBoard the 2d-array chessboard
     */
    public void generatePossibleMoves(Piece p, ArrayList<Piece> whiteP, ArrayList<Piece> blackP, String[][] chBoard){
        String[][] holder = chBoard;
        /**
         * gets its position on the chess board
         */
        String currentSTR = p.getposString();
        /**
         * gets its color
         */
        String color = p.getColor();

        /**
         * gets its current x coordinate
         */
        int currentX = posGetX_fromString(currentSTR);
        /**
         * gets its current y coordinate
         */
        int currentY = posGetY_fromString(currentSTR);

        /**
         * gets the position on the chessboard that is legal for the current
         * pawn to move to
         */
        String temp = "";
        /**
         * count if the other pieces on the same team are not in the way
         * of the chess spot.
         */
        int count = 0;

        if(color == "white"){
            /*
            check if the indexed white piece is in front of the current pawn
            */
            for(Piece x: whiteP){
                // checking all white pieces besides the current piece's coordinates
                if((x.getYpos() != (currentY +1) || x.getXpos() != currentX) && x.getposString() != currentSTR){
                    count++; // if no white piece in the target position... +1
                }
            }
            for (Piece z : blackP){ //check all black pieces to make sure not are directly in front
                if((z.getYpos() != (currentY +1) || z.getXpos() != currentX))
                    count++;
            }
            //System.out.println(count);

            if (count == ((whiteP.size()-1)) + blackP.size()){ // if there are none white pieces or black pieces in that target position
                temp = getStringPos_fromXY(currentX, currentY + 1);
                p.setListPossMoves(temp);
            }

            count = 0;
            /*
            check if the indexed white piece is 2 space front of the current pawn
            */
            for(Piece x: whiteP){
                if((x.getYpos() != (currentY + 2) || x.getXpos() != currentX) && currentY == 1 && x.getposString() != currentSTR){
                    count++;
                }
            }

            for (Piece z : blackP){ //check all black pieces to make sure not 2 spaces directly in front
                if(((z.getYpos() != (currentY +2) || z.getXpos() != currentX)) && currentY == 1)
                    count++;
            }

            if(count == ((whiteP.size()-1)) + blackP.size()){
                temp = getStringPos_fromXY(currentX, currentY + 2);
                p.setListPossMoves(temp);
            }
        }


        else if(color == "black"){
            for(Piece x: blackP){
                /**
                 * checking all black pieces besides the current piece's coordinates
                 */
                if((x.getYpos() != (currentY - 1) || x.getXpos() != currentX) && x.getposString() != currentSTR){
                    count++; // if no white black in the target position... +1
                }
            }
            /**
             * check all white pieces to make sure not are directly in front
             */
            for (Piece z : whiteP){
                if((z.getYpos() != (currentY -1) || z.getXpos() != currentX))
                    count++;
            }

            if (count == (whiteP.size() + (blackP.size()-1))){ // if there are none black pieces in that target position
                temp = getStringPos_fromXY(currentX, currentY - 1);
                p.setListPossMoves(temp);
            }

            count = 0;
            /**
             * check if the indexed black piece is 2 space front of the current pawn
             */
            for(Piece x: blackP){
                if((x.getYpos() != (currentY - 2) || x.getXpos() != currentX) && currentY == 6 && x.getposString() != currentSTR){
                    count++;
                }
            }
            /**
             * check all white pieces to make sure not 2 spaces directly in front
             */
            for (Piece z : whiteP){
                if(((z.getYpos() != (currentY - 2) || z.getXpos() != currentX)) && currentY == 6)
                    count++;
            }

            if(count == ((blackP.size()-1)) + whiteP.size()){
                temp = getStringPos_fromXY(currentX, currentY - 2);
                p.setListPossMoves(temp);
            }

        }


    }
    /**
     * List of possible targets to take out on the chess board.
     * Checks if it is opposite colors by using legal movement on the board.
     *<p>
     * @param p the piece that is attacking
     * @param whiteP array of white pieces
     * @param blackP array of black pieces
     * @param chBoard 2d array that is the chess board
     */
    public void generatePossibleAttacks(Piece p, ArrayList<Piece> whiteP, ArrayList<Piece> blackP, String[][] chBoard){

        String currentSTR = p.getposString();
        String color = p.getColor();

        int currentX = posGetX_fromString(currentSTR);
        int currentY = posGetY_fromString(currentSTR);


        String temp = "";

        if(color == "white"){
            for(Piece x: blackP){
                if(x.getYpos() == currentY + 1 && x.getXpos() == currentX + 1){
                    temp = getStringPos_fromXY(currentX + 1, currentY + 1);
                    p.setListPossAttacks(temp);
                    break;
                }
            }
            for(Piece x: blackP){
                if(x.getYpos() == currentY + 1 && x.getXpos() == currentX - 1){
                    temp = getStringPos_fromXY(currentX - 1, currentY + 1);
                    p.setListPossAttacks(temp);
                    break;
                }
            }
        }

        else if(color == "black"){
            for(Piece x: whiteP){
                if(x.getYpos() == currentY - 1 && x.getXpos() == currentX + 1){
                    temp = getStringPos_fromXY(currentX + 1, currentY - 1);
                    p.setListPossAttacks(temp);
                    break;
                }
            }
            for(Piece x: whiteP){
                if(x.getYpos() == currentY - 1 && x.getXpos() == currentX - 1){
                    temp = getStringPos_fromXY(currentX - 1, currentY - 1);
                    p.setListPossAttacks(temp);
                    break;
                }
            }
        }
    }

    /**
     * Uses either the possible move list to move selected chess piece
     * to new position or use the possible attack list to replace
     * a opposing chess piece
     * <p>
     * @param currentPos the position of the piece that is attacking
     * @param targetPos the position of the piece getting attacked
     * @param colorTurn the attacking player's side
     * @param p the current chess piece
     * @param whitePcs array of white pieces
     * @param blackPcs array of black pieces
     * @return true if piece has been moved
     */
    public boolean move(String currentPos, String targetPos, String colorTurn, Piece p, ArrayList<Piece> whitePcs,
                        ArrayList<Piece> blackPcs){
        //System.out.println(p.getposString() + "----");
        String pieceColor = p.getColor();
        /**
         * check if target piece's color is not same as current color turn
         */
        if(pieceColor != colorTurn){
            return false;
        }
        /**
         Sets the target position as the current position if the target position is in the possilbe list
         */
        if(p.getPossMovesList().contains(targetPos)){
            p.setOldPos(currentPos);
            p.setposString(targetPos);
            int j = posGetX_fromString(targetPos);
            int k = posGetY_fromString(targetPos);
            p.setXpos(j);
            p.setYpos(k);
            p.clearListPossMoves();
            return true;
        }

        /**
         Replace the target piece and takes it's position if the target position is in the possilbe list
         */
        if(p.getPossAttacksList().contains(targetPos)){


            if(colorTurn.equals("white")){

                Iterator<Piece> itr = blackPcs.iterator();
                while (itr.hasNext()) {
                    Piece z = itr.next();
                    if (z.getposString().equals(targetPos))  {
                        itr.remove();
                    }
                }


            }
            else if (colorTurn.equals("black")){
                Iterator<Piece> itr2 = whitePcs.iterator();
                while (itr2.hasNext()) {
                    Piece y = itr2.next();
                    if (y.getposString().equals(targetPos))  {
                        itr2.remove();
                    }
                }

            }

            /**
             * update the old position of the chess piece with the new one.
             */
            p.setOldPos(currentPos);
            p.setposString(targetPos);
            int a = posGetX_fromString(targetPos);
            int b = posGetY_fromString(targetPos);
            p.setXpos(a);
            p.setYpos(b);
            p.clearListPossAttacks();


            return true;
        }

        return false;

        //convert target position to x,y coordinates
        //find all 4 possible moves
        //check if new target position is one of these possible moves
        //if not, return false in this method, then in application print error message and promt user for new move
        //if it can work

    }

}
