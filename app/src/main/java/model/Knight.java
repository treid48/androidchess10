package model;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author David Zhu, Tim Reid
 * The Knight class where we design how the pawn interacts with others chess pieces
 * and it's behavior, attributes and limitations.
 *
 */

public class Knight extends Piece {

    /**
     * @param name the chess piece role
     * @param color the player's side
     * @param posString the chess board position of the piece
     * @param xPos the x-coordinate/row number of the 2-d array for that piece based off the chess board
     * @param yPos the y-coordinate/row number of the 2-d array for that piece based off the chess board
     */
    public Knight(String name, String color, String posString, int xPos, int yPos){  //basic cosntructor
        super(name, color, posString, xPos, yPos);
        oldPos = "00";
    }
    /**
     * Generate a list containing the possible chess board positions the
     * Knight can move to.
     * <p>
     * @param p the chess piece
     * @param whiteP list of white pieces present on the board
     * @param blackP list of black pieces present on the board
     * @param chBoard the 2d-array chessboard
     */
    public void generatePossibleMoves(Piece p, ArrayList<Piece> whiteP, ArrayList<Piece> blackP, String[][] chBoard){
        /**
         * gets its position on the chess board
         */
        String currentSTR = p.getposString();
        /**
         * gets its color
         */
        String color = p.getColor();
        /**
         * gets its current x coordinate
         */
        int currentX = posGetX_fromString(currentSTR);
        /**
         * gets its current y coordinate
         */
        int currentY = posGetY_fromString(currentSTR);
        /**
         * gets the position on the chessboard that is legal for the current
         * pawn to move to
         */
        String temp = "";

        /**
         * Make an array for the possible moves indexes of the knight's current
         * position.
         * It is to be looped to check if target index is in bound and if there
         * is no piece already present.
         * It starts from clockwise.
         * <p>
         * up 2 - right 1, index 0
         * up 1 - right 2, index 1
         * down 2 - right 1, index 2
         * down 1 - right 2, index 3
         * down 2 - left 1, index 4
         * down 1 - left 2, index 5
         * up 1 - left 2, index 6
         * up 2 - left 1, index 7
         */

        int indexX[] = { 1, 2, 1, 2, -1, -2, -2, -1 };
        int indexY[] = { 2, 1, -2, -1, -2, -1, 1, 2 };

        for(int i = 0; i < 8; i ++) {
            int newX = currentX + indexX[i];
            int newY = currentY + indexY[i];
            if(newX < 8 && newX >= 0 && newY < 8 && newY >= 0) {
                if(chBoard[newX][newY].equals("  ") || chBoard[newX][newY].equals("##")){
                    temp = getStringPos_fromXY(newX, newY);
                    p.setListPossMoves(temp);
                }
            }
        }
    }
    /**
     * List of possible targets to take out on the chess board.
     * Checks if it is opposite colors by using legal movement on the board.
     *<p>
     * @param p the piece that is attacking
     * @param whiteP array of white pieces
     * @param blackP array of black pieces
     * @param chBoard 2d array that is the chess board
     */
    public void generatePossibleAttacks(Piece p, ArrayList<Piece> whiteP, ArrayList<Piece> blackP, String[][] chBoard){
        String currentSTR = p.getposString(); // String placement on board
        String color = p.getColor();
        String temp = "";
        int currentX = posGetX_fromString(currentSTR);
        int currentY = posGetY_fromString(currentSTR);
        /**
         * Make an array for the possible moves indexes of the knight's current
         * position.
         * It is to be looped to check if target index is in bound and if there
         * is no piece already present.
         * It starts from clockwise.
         * <p>
         * up 2 - right 1, index 0
         * up 1 - right 2, index 1
         * down 2 - right 1, index 2
         * down 1 - right 2, index 3
         * down 2 - left 1, index 4
         * down 1 - left 2, index 5
         * up 1 - left 2, index 6
         * up 2 - left 1, index 7
         */

        int indexX[] = { 1, 2, 1, 2, -1, -2, -2, -1 };
        int indexY[] = { 2, 1, -2, -1, -2, -1, 1, 2 };

        for(int i = 0; i < 8; i ++) {
            int newX = currentX + indexX[i];
            int newY = currentY + indexY[i];
            if(newX < 8 && newX >= 0 && newY < 8 && newY >= 0) {
                if(color.equals("white") && (chBoard[newX][newY].equals("bp") || chBoard[newX][newY].equals("bN") || chBoard[newX][newY].equals("bB") || chBoard[newX][newY].equals("bQ") || chBoard[newX][newY].equals("bK") || chBoard[newX][newY].equals("bR")  )){
                    temp = getStringPos_fromXY(newX, newY);
                    p.setListPossAttacks(temp);
                    break;
                }   else if ( color.equals("black") && (chBoard[newX][newY].equals("wp") || chBoard[newX][newY].equals("wN") || chBoard[newX][newY].equals("wB") || chBoard[newX][newY].equals("wQ") || chBoard[newX][newY].equals("wK") || chBoard[newX][newY].equals("wR") ) ){
                    temp = getStringPos_fromXY(newX, newY);
                    p.setListPossAttacks(temp);
                    break;
                }
            }
        }

    }
    /**
     * Uses either the possible move list to move selected chess piece
     * to new position or use the possible attack list to replace
     * a opposing chess piece
     * <p>
     * @param currentPos the position of the piece that is attacking
     * @param targetPos the position of the piece getting attacked
     * @param colorTurn the attacking player's side
     * @param p the current chess piece
     * @param whitePcs array of white pieces
     * @param blackPcs array of black pieces
     * @return true if piece has been moved
     */

    public boolean move(String currentPos, String targetPos, String colorTurn, Piece p, ArrayList<Piece> whitePcs,
                        ArrayList<Piece> blackPcs){
        if(p.getPossMovesList().contains(targetPos)){
            p.setOldPos(currentPos);
            p.setposString(targetPos);
            int j = posGetX_fromString(targetPos);
            int k = posGetY_fromString(targetPos);
            p.setXpos(j);
            p.setYpos(k);
            p.clearListPossMoves();
            return true;
        }

        if(p.getPossAttacksList().contains(targetPos)){


            if(colorTurn.equals("white")){

                Iterator<Piece> itr = blackPcs.iterator();
                while (itr.hasNext()) {
                    Piece z = itr.next();
                    if (z.getposString().equals(targetPos))  {
                        itr.remove();
                    }
                }


            }
            else if (colorTurn.equals("black")){
                Iterator<Piece> itr2 = whitePcs.iterator();
                while (itr2.hasNext()) {
                    Piece y = itr2.next();
                    if (y.getposString().equals(targetPos))  {
                        itr2.remove();
                    }
                }

            }

            p.setOldPos(currentPos);
            p.setposString(targetPos);
            int a = posGetX_fromString(targetPos);
            int b = posGetY_fromString(targetPos);
            p.setXpos(a);
            p.setYpos(b);
            p.clearListPossAttacks();



            return true;
        }

        return false;
    }

}